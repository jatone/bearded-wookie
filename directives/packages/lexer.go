package packages

import (
	"fmt"
	"log"
	"strings"
	"unicode/utf8"
)

type tokenType int

const (
	tokenInit tokenType = iota
	tokenText
	tokenFin
)

func (t tokenType) String() string {
	switch t {
	case tokenInit:
		return "INIT"
	case tokenFin:
		return "FIN"
	case tokenText:
		return "text"
	default:
		return "unknown"
	}
}

const eof = -2
const separator = ';'
const escape = '\''

// token representing either a control sequence or text.
type token struct {
	typ    tokenType
	offset int
	val    string
	err    error
}

func (t token) String() string {
	return t.val
}

// lex builds a lexer from the provided string.
func lex(input string) (l *lexer) {
	l = &lexer{
		input:          input,
		nextStartState: lexInit,
	}
	return l
}

type stateFn func(*lexer) stateFn

// lexer search language lexer.
type lexer struct {
	input          string  // input into the Lexer
	start          int     // start position of this item
	pos            int     // current position in the input
	width          int     // width of last rune read from input
	state          stateFn // state of the Lexer
	nextStartState stateFn // the state the Lexer should start with on invocation of NextToken
	result         token   // the token that will be returned by NextToken
}

// NextToken returns the next token generated by the lexer.
func (t *lexer) NextToken() token {
	for t.state = t.nextStartState; t.state != nil; {
		t.state = t.state(t)
	}

	return t.result
}

// advances the current position by the width of the rune.
func (t *lexer) next() (r rune) {
	if t.pos >= len(t.input) {
		t.width = 0
		return eof
	}

	r, t.width = utf8.DecodeRuneInString(t.input[t.pos:])
	t.pos += t.width
	return r
}

// returns true iff the start position does not equal the current position.
func (t *lexer) hasAdvanced() bool {
	return t.start != t.pos
}

// returns true iff the lexer is at the end of the input string.
func (t *lexer) isEOF() bool {
	return t.peek() == eof
}

// moves the current position back by the last rune read. DO NOT invoke repeatedly.
// only works for the last rune read. in fact just don't invoke this directly.
func (t *lexer) backup() {
	t.pos -= t.width
}

// returns the next rune that would be read.
func (t *lexer) peek() rune {
	r := t.next()
	t.backup()
	return r
}

// will assert the next rune is within the valid set without advancing the buffer.
func (t *lexer) assertNext(valid string) bool {
	return strings.ContainsRune(valid, t.peek())
}

// will accept a single rune from the buffer
// if it is in the set of valid runes provided in the string.
func (t *lexer) accept(valid string) bool {
	if strings.IndexRune(valid, t.next()) >= 0 {
		return true
	}
	t.backup()
	return false
}

// will continuously accept runes from the buffer
// if they are within the set of valid runes.
func (t *lexer) acceptRun(valid string) {
	for strings.IndexRune(valid, t.next()) >= 0 {
	}
	t.backup()
}

// will continue to accept runes until it finds a rune in the abort runes
func (t *lexer) acceptUntil(abort string) bool {
	for !(t.isEOF() || t.assertNext(abort)) {
		t.next()
	}

	return t.hasAdvanced()
}

// advances the pointer effectively ignoring the runes
// between start and pos.
func (t *lexer) advance(b bool) bool {
	if b {
		t.start = t.pos
	}
	return b
}

// package;version;;extra
func lexText(l *lexer) stateFn {
	if l.acceptUntil("';") {
		return emitToken(tokenText, maybeQuote(lexBreak))
	}

	// no additional texts
	if l.isEOF() {
		return emitToken(tokenFin, nil)
	}

	return lexBreak
}

func maybeQuote(next stateFn) stateFn {
	return func(l *lexer) stateFn {
		l.advance(l.accept("'"))
		return next
	}
}

func lexBreak(l *lexer) stateFn {
	// inspect(l, "lexBreak start")
	// defer inspect(l, "lexBreak fin")
	l.advance(l.accept(";"))

	switch l.peek() {
	case ';':
		return maybeFin(emitToken(tokenText, lexBreak))
	default:
		return maybeQuote(lexText)
	}
}

func maybeFin(next stateFn) stateFn {
	return func(l *lexer) stateFn {
		if l.isEOF() {
			return emitToken(tokenFin, nil)
		}

		return next
	}
}

func lexInit(l *lexer) stateFn {
	// inspect(l, "lexInit begin")
	// defer inspect(l, "lexInit fin")

	switch r := l.next(); r {
	case '\'':
		l.advance(true)
		return lexInit
	case ';':
		l.advance(true)
		return emitToken(tokenText, lexInit)
	default:
	}

	return maybeQuote(lexText)
}

func emitToken(typ tokenType, nextStartState stateFn) stateFn {
	return func(l *lexer) stateFn {
		l.nextStartState = nextStartState
		l.result = token{typ: typ, val: l.input[l.start:l.pos], offset: l.start}
		l.start = l.pos
		return nil
	}
}

func inspect(l *lexer, msg string) {
	log.Println(
		msg,
		fmt.Sprintf("%s%s|%s", l.input[:l.start], l.input[l.start:l.pos], l.input[l.pos:]),
		"state:",
		l.result.typ, l.result.val,
	)
}
