package quorum

import (
	"bytes"
	"context"
	"hash"
	"io"
	"log"
	"sync"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	bw "bitbucket.org/jatone/bearded-wookie"
	"bitbucket.org/jatone/bearded-wookie/agent"
	"bitbucket.org/jatone/bearded-wookie/clustering/raftutil"
	"bitbucket.org/jatone/bearded-wookie/storage"
	"bitbucket.org/jatone/bearded-wookie/x/debugx"
	"bitbucket.org/jatone/bearded-wookie/x/grpcx"
	"github.com/hashicorp/memberlist"
	"github.com/hashicorp/raft"
	"github.com/pkg/errors"
)

type cluster interface {
	Local() agent.Peer
	Quorum() []agent.Peer
	LocalNode() *memberlist.Node
	Get([]byte) *memberlist.Node
	GetN(int, []byte) []*memberlist.Node
}

type deploy interface {
	Deploy(int64, grpc.DialOption, agent.Archive, ...agent.Peer)
}

// errorFuture is used to return a static error.
type errorFuture struct {
	err error
}

func (e errorFuture) Error() error {
	return e.err
}

func (e errorFuture) Response() interface{} {
	return nil
}

func (e errorFuture) Index() uint64 {
	return 0
}

type pbObserver struct {
	dst  agent.Quorum_WatchServer
	done context.CancelFunc
}

func (t pbObserver) Receive(messages ...agent.Message) (err error) {
	var (
		cause error
	)

	for _, m := range messages {
		if err = t.dst.Send(&m); err != nil {
			if cause = errors.Cause(err); cause == context.Canceled {
				return nil
			}

			t.done()

			if grpcx.IgnoreShutdownErrors(cause) == nil {
				return nil
			}

			return errors.Wrapf(err, "error type %T", cause)
		}
	}

	return nil
}

type raftproxy interface {
	State() raft.RaftState
	Leader() raft.ServerAddress
	Apply([]byte, time.Duration) raft.ApplyFuture
}

type proxyRaft struct {
}

func (t proxyRaft) State() raft.RaftState {
	return raft.Shutdown
}

func (t proxyRaft) Leader() raft.ServerAddress {
	return ""
}

func (t proxyRaft) Apply(cmd []byte, d time.Duration) raft.ApplyFuture {
	return errorFuture{err: errors.New("apply cannot be executed on a proxy node")}
}

type proxyDispatch struct {
	dialOptions []grpc.DialOption
	peader      agent.Peer
	client      agent.Conn
	o           *sync.Once
}

func (t *proxyDispatch) Dispatch(m agent.Message) (err error) {
	t.o.Do(func() {
		if t.client, err = agent.Dial(agent.RPCAddress(t.peader), t.dialOptions...); err != nil {
			t.o = &sync.Once{}
		}
	})

	if err != nil {
		return errors.Wrap(err, "proxy dispatch dial failure")
	}

	return errors.Wrapf(t.client.Dispatch(m), "proxy dispatch: %s - %s", t.peader.Name, t.peader.Ip)
}

func (t *proxyDispatch) close() {
	t.client.Close()
}

// Option option for the quorum rpc.
type Option func(*Quorum)

// OptionUpload set the upload storage protocol.
func OptionUpload(proto storage.Protocol) Option {
	return func(q *Quorum) {
		q.uploads = proto
	}
}

// OptionCredentials set the dial credentials options for the agent.
// when the credentials are nil then insecure connections are used.
func OptionCredentials(c credentials.TransportCredentials) Option {
	return func(q *Quorum) {
		if c == nil {
			q.creds = grpc.WithInsecure()
		} else {
			q.creds = grpc.WithTransportCredentials(c)
		}
	}
}

// New new quorum instance based on the options.
func New(c cluster, d deploy, options ...Option) Quorum {
	r := Quorum{
		stateMachine: NewStateMachine(),
		uploads: storage.ProtocolFunc(
			func(uid []byte, _ uint64) (storage.Uploader, error) {
				return storage.NewTempFileUploader()
			},
		),
		creds:  grpc.WithInsecure(),
		m:      &sync.Mutex{},
		proxy:  proxyRaft{},
		pq:     proxyDispatch{},
		deploy: d,
		c:      c,
	}

	for _, opt := range options {
		opt(&r)
	}

	return r
}

// Quorum implements quorum functionality.
type Quorum struct {
	stateMachine *StateMachine
	uploads      storage.Protocol
	m            *sync.Mutex
	c            cluster
	creds        grpc.DialOption
	proxy        raftproxy
	pq           proxyDispatch
	deploy       deploy
}

// Observe observes a raft cluster and updates the quorum state.
func (t *Quorum) Observe(rp raftutil.Protocol, events chan raft.Observation) {
	go rp.Overlay(
		t.c,
		raftutil.ProtocolOptionStateMachine(func() raft.FSM {
			return t.stateMachine
		}),
		raftutil.ProtocolOptionObservers(
			raft.NewObserver(events, true, func(o *raft.Observation) bool {
				switch o.Data.(type) {
				case raft.LeaderObservation, raft.RaftState:
					return true
				default:
					return false
				}
			}),
		),
	)

	for o := range events {
		switch o.Data.(type) {
		case raft.LeaderObservation:
			t.pq.close()

			if o.Raft.Leader() == "" {
				debugx.Println("leader lost disabling quorum locally")
				t.proxy = proxyRaft{}
				t.pq = proxyDispatch{}
				continue
			}

			peader := t.findLeader(string(o.Raft.Leader()))
			debugx.Println("leader identified, enabling quorum locally", peader.Name, peader.Ip)
			t.proxy = o.Raft
			t.pq = proxyDispatch{
				peader: peader,
				o:      &sync.Once{},
				dialOptions: []grpc.DialOption{
					t.creds,
				},
			}
		}
	}
}

// Deploy ...
func (t *Quorum) Deploy(concurrency int64, archive agent.Archive, peers ...agent.Peer) (err error) {
	var (
		c agent.Client
	)

	t.m.Lock()
	p := t.proxy
	t.m.Unlock()

	debugx.Println("deploy invoked")
	defer debugx.Println("deploy completed")

	switch s := p.State(); s {
	case raft.Leader:
		debugx.Println("proxy deploy initiated")
		t.deploy.Deploy(concurrency, t.creds, archive, peers...)
		debugx.Println("proxy deploy completed")
		return err
	default:
		debugx.Println("forwarding deploy request to leader")
		if c, err = agent.Dial(agent.RPCAddress(t.findLeader(string(p.Leader()))), t.creds); err != nil {
			return err
		}

		defer c.Close()
		return c.RemoteDeploy(concurrency, archive, peers...)
	}
}

// Upload ...
func (t *Quorum) Upload(stream agent.Quorum_UploadServer) (err error) {
	var (
		deploymentID []byte
		checksum     hash.Hash
		location     string
		dst          agent.Uploader
		chunk        *agent.ArchiveChunk
	)
	t.m.Lock()
	p := t.proxy
	t.m.Unlock()

	debugx.Println("upload invoked")
	defer debugx.Println("upload completed")

	switch s := p.State(); s {
	case raft.Leader, raft.Follower, raft.Candidate:
	default:
		log.Println("failed upload not a member of quorum", s.String(), p.Leader())
		return errors.Errorf("upload must be run on a member of quorum: %s", s)
	}

	debugx.Println("upload: generating deployment ID")
	if deploymentID, err = bw.GenerateID(); err != nil {
		return err
	}

	debugx.Println("upload: receiving metadata")
	if chunk, err = stream.Recv(); err != nil {
		return errors.WithStack(err)
	}

	debugx.Printf("upload: initializing protocol: %T\n", t.uploads)
	if dst, err = t.uploads.NewUpload(deploymentID, chunk.GetMetadata().Bytes); err != nil {
		return err
	}

	for {
		chunk, err := stream.Recv()

		if err == io.EOF {
			if checksum, location, err = dst.Info(); err != nil {
				log.Println("error getting archive info", err)
				return err
			}

			tmp := t.c.Local()
			return stream.SendAndClose(&agent.Archive{
				Peer:         &tmp,
				Location:     location,
				Checksum:     checksum.Sum(nil),
				DeploymentID: deploymentID,
				Ts:           time.Now().UTC().Unix(),
			})
		}

		if err != nil {
			log.Println("error receiving chunk", err)
			return err
		}

		debugx.Println("upload: chunk received")
		if checksum, err = dst.Upload(bytes.NewBuffer(chunk.Data)); err != nil {
			log.Println("error uploading chunk", err)
			return err
		}
	}
}

// Watch watch for events.
func (t *Quorum) Watch(out agent.Quorum_WatchServer) (err error) {
	t.m.Lock()
	p := t.proxy
	t.m.Unlock()

	debugx.Println("watch invoked")
	defer debugx.Println("watch completed")

	switch s := p.State(); s {
	case raft.Leader, raft.Follower, raft.Candidate:
	default:
		return errors.Errorf("watch must be run on a member of quorum: %s", s)
	}

	ctx, done := context.WithCancel(context.Background())

	debugx.Println("event observer: registering")
	o := t.stateMachine.Register(pbObserver{dst: out, done: done})
	debugx.Println("event observer: registered")
	defer t.stateMachine.Remove(o)

	<-ctx.Done()

	return nil
}

// Dispatch record deployment events.
func (t *Quorum) Dispatch(in agent.Quorum_DispatchServer) error {
	var (
		err error
		m   *agent.Message
	)
	debugx.Println("dispatch initiated")
	defer debugx.Println("dispatch completed")
	t.m.Lock()
	p := t.proxy
	dispatch := t.pq.Dispatch
	t.m.Unlock()

	if p.State() == raft.Leader {
		dispatch = func(m agent.Message) error {
			return maybeApply(p, 5*time.Second)(MessageToCommand(m)).Error()
		}
	}

	for m, err = in.Recv(); err == nil; m, err = in.Recv() {
		if err = dispatch(*m); err != nil {
			return err
		}
	}

	return nil
}

func (t Quorum) findLeader(leader string) (_zero agent.Peer) {
	var (
		peader agent.Peer
	)

	for _, peader = range t.c.Quorum() {
		if agent.RaftAddress(peader) == leader {
			return peader
		}
	}

	log.Println("-------------------- failed to locate leader --------------------")
	return _zero
}

func maybeApply(p raftproxy, d time.Duration) func(cmd []byte, err error) raft.ApplyFuture {
	return func(cmd []byte, err error) raft.ApplyFuture {
		return p.Apply(cmd, d)
	}
}
