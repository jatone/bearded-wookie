#### modules
- modules allow for organizing the directives in a hierarchical manner.
- will use folders to represent a module. all the directives within the folder (including any child modules)
are executed in order. once all directives are executed it will return
up the stack and continue running siblings.
